import React, { useEffect, useState } from "react";
import { styleGridItem, styleGrid } from "../../styles";

import ListComponent from "../../components/listComponent";
import CardInput from "../../components/cardInput";
import { usersWithReversedNames, reverseString, isPalindrome, padNumberWithZeroes, findNthLargestNumber } from "../../service"; 


const Home = () => {


    const [users, setUsers] = useState([]);
    const [reverse, setReverse] = useState('');
    const [palindrome, setPalindrome] = useState('');
    const [padNumber, setPadNumber] = useState(0);
    const [nthNumber, setNthNumber] = useState(0);
    const [numbers, setNumbers] = useState([]);
    const [number, setNumber] = useState(0);

    const [answerReverse, setAnswerReverse] = useState('');
    const [answerIsPalindrome, setAnswerIsPalindrome] = useState();
    const [answerPadnumber, setAnswerPadNumber] = useState(0);
    const [answerNthNumber, setAnswerNthNumber] = useState(0);

    const loadUsers = () => {
        usersWithReversedNames().then(res =>{
            setUsers(res.data)
        }).catch(err => console.log(err))
    }

    const onChangeReverse = (event) => {
        setReverse(event.target.value)
    }

    const onChangePalindrome = (event) => {
        setPalindrome(event.target.value);
    }
    const onChangePadNumber = (event) => {
        setPadNumber(event.target.value)
    }
    const onChangeNthNumber = (event) => {
        setNthNumber(event.target.value)
    }
    const onChangeNumbers = (event) => {
        setNumber(event.target.value)
    }

    const onClickNumber = () =>{
        setNumbers(oldValue => [...oldValue, Number(number)]);
        setNumber(0)
    }

    const onClickPalindrome = () => {
        isPalindrome(palindrome).then(res => {
            setPalindrome('');
            setAnswerIsPalindrome(res.data)
        }).catch(err => console.log(err))
    }

    const onClickReverse = () => {
        reverseString(reverse).then(res => {
            setReverse('')
            setAnswerReverse(res.data)
        })
        .catch(err => console.log(err))
    }

    const onClickPadNumber = () => {
        padNumberWithZeroes(padNumber).then(res => {
            setPadNumber(0)
            setAnswerPadNumber(res.data)
        }).catch(err => console.log(err))
    }

    const onClickNthNumber = () => {
        findNthLargestNumber(numbers, nthNumber).then(res => {
            setNthNumber(0)
            setAnswerNthNumber(res.data)
        }).catch(err => console.log(err))
    }

    const onClickClearNumbers = () => {
        setNumbers([])
    }

    useEffect(() => {
        loadUsers();   
    }, [])

    return(
        <div>
            <h1>Home page</h1>
            
            <ListComponent 
                value={users}
                itemstype="object"/>

            <div style={styleGrid}>
                <div style={styleGridItem}>
                    <CardInput 
                        onClickButton={onClickReverse} 
                        value={reverse} 
                        onChangeValue={onChangeReverse}
                        type="text"
                        buttonText="Reverse string"
                        answer={answerReverse}
                        description="Enter a value"
                    />
                    <CardInput 
                        onClickButton={onClickPalindrome}
                        value={palindrome}
                        onChangeValue={onChangePalindrome}
                        type="text"
                        buttonText="Check if palindrome"
                        answer={answerIsPalindrome}
                        description="Enter a value"
                    />

                    <CardInput 
                        onClickButton={onClickPadNumber}
                        value={padNumber}
                        onChangeValue={onChangePadNumber}
                        type="number"
                        buttonText="Add zeroes"
                        answer={answerPadnumber}
                        description="Enter a number"
                    />

                    <CardInput 
                        onClickButton={onClickNumber}
                        value={number}
                        onChangeValue={onChangeNumbers}
                        type="number"
                        buttonText="Add numbers"
                        description="Add numbers to list"      
                        ignoreAnswer
                    />

                    <CardInput
                        value={nthNumber}
                        onChangeValue={onChangeNthNumber}
                        type="number" 
                        answer={answerNthNumber}
                        description="Enter the nth position"
                        />

                    
                    <CardInput
                        onClickButton={onClickNthNumber}
                        buttonText="Check answer"
                        answer={answerNthNumber}
                        />
                </div>
            <div style={styleGridItem}>
                <ListComponent value={numbers} />
                <CardInput 
                    onClickButton={onClickClearNumbers}
                    buttonText="Clear numbers"
                    ignoreAnswer
                />
            </div>
            </div>
            
            
            
            {/* {answerNthNumber ? (<div>{answerNthNumber}</div>): (<span></span>)} */}
        </div>
    )
}

export default Home;