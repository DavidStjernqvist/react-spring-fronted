export const styleGrid = {
    display: "grid",
    gridColumnGap: "50px",
    gridTemplateColumns: "auto auto",
    padding: "10px" 
}
export const styleGridItem = {
    padding: "20 px"
}

export const styleListIsPalindrome = {
    backgroundColor: "green",
    color: "white",
    listStyleType: "none",
}

export const styleListIsNotPalindrome = {
    backgroundColor: "white",
    color: "black",
    listStyleType:"none"
}

export const styleListTypeNone = {
    listStyleType: "none"
}

export const styleButton = {
    width: "100%",
    backgroundColor: "blue",
    color: "white",
    border: "none",
    borderRadius: "4 px",
    cursor: "pointer"
}

export const styleInput = {
    width: "98.5%",
    display: "inline-block",
    height: "20px",
    borderRadius: "4px",
    bozSizing: "border-box"
}

export const styleCard = {
    padding: "15px 100px 0px 30px"
}
