import axios from 'axios';

const url = process.env.REACT_APP_BACKEND_HOST_URL || "http://localhost:8080";

export const usersWithReversedNames = () => axios.get(`${url}/api/userswithreversednames`)

export const reverseString = (string) => axios.get(`${url}/api/reversestring`, {params: {string: string}})

export const isPalindrome = (string) => axios.get(`${url}/api/ispalindrome`, {params: {string: string}})

export const padNumberWithZeroes = (number) => axios.get(`${url}/api/padnumberwithzeroes`, {params: {number: number}})

export const findNthLargestNumber = (numbers, nthLargestNumber) => axios.get(`${url}/api/findnthlargestnumber`, {
                                                                        params: {
                                                                            numbers: String(numbers),
                                                                            nthlargestnumber: nthLargestNumber
                                                                        }})