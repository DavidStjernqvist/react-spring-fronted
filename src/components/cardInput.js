import React, {useState} from 'react';

import { styleButton, styleInput, styleCard } from "../styles";

const CardInput = ({onClickButton, value, onChangeValue, type, buttonText, answer, ignoreAnswer, description}) => {
    
    const [flipCard, setFlipCard] = useState(false);
    
    const onClick = () => {
        try{
            onClickButton()
            setFlipCard(ignoreAnswer ? flipCard : true)
        }catch (e){
            console.log(e)
        }
    }

    const setFlipCardFalse = () => {
        setFlipCard(false)
    }

    const printEmpty = () => (<span></span>)

    const printButton = (onClickButton) => onClickButton ? (
        <div>
            <button onClick={onClick} style={styleButton}>{buttonText}</button>
        </div>
    ) : printEmpty()

    const printAnswer = (answer) => typeof answer === "boolean" ? (
        <div>{String(answer)}</div>
    ) : answer ? (
        <div>{answer}</div>
    ) : printEmpty()

    const printInput = (type, value, onChangeValue) => type ? (
        <input style={styleInput} type={type} value={value} onChange={onChangeValue} />
    ) : printEmpty()

    const printDescription = (description) => description ? (
        <div>{description}</div>
    ): printEmpty()

    return (
        <div style={styleCard}>
        {flipCard ? (
        <div>
            <div>{printAnswer(answer)}</div>
            <button onClick={setFlipCardFalse}>Back</button>
        </div>
        
        ) : (
        <div>
            {printDescription(description)}
            {printInput(type, value, onChangeValue)}
            {printButton(onClickButton)}    
        </div>    
            
        )}
        </div>
    )
}

export default CardInput