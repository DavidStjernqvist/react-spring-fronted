import React from 'react';
import { styleListIsNotPalindrome, styleListIsPalindrome, styleListTypeNone } from "../styles";
const ListComponent = ({value, itemstype}) => {

    const stylePalindrome = (isPalindrome) => isPalindrome ? 
        styleListIsPalindrome : styleListIsNotPalindrome

    const printItems = (value, itemstype) => itemstype === "object" ? 
        value.map((user, index) => (
            <li key={index} style={stylePalindrome(user.isPalindrome)}>
                {user.name}
            </li>)) : 
        value.map((parameter, index) => (
            <li key={index} style={styleListTypeNone}>
                {parameter}
            </li>
    ))
    return(
        <div>
            <ul>
                {printItems(value, itemstype)}
            </ul>
        </div>
        
    )
}

export default ListComponent