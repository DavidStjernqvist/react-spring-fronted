import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./views/homeView"

const App = () => {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/">
            <Home />
          </Route>  
        </Switch>  
      </Router>      
    </div>
  );
}

export default App;
